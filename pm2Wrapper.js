const pm2 = require('pm2');
let _socket;

const setupSocket = (socket) => {
    _socket = socket;
}

const list = (props) => {
    return new Promise((resolve, reject) => {

        const errList = (err, list) => {
            try {

                if (err) {
                    _socket?.send(JSON.stringify({
                        status: "error",
                        message: "could not execute list",
                        error: err.toString(),
                        id: props.id
                    }));

                    reject(new Error("list: PM2 error received"));

                } else {
                    _socket.send(JSON.stringify({
                        status: "ok",
                        cmd: "list",
                        result: {
                            list: list,
                        },
                        id: props.id
                    }));

                    resolve({ status: "ok" });
                }
            } catch (error) {
                console.log("errList::error -->", error);
                reject(new Error("list error"));
            }
        }

        pm2.list(errList);

    })
}

const start = (props) => {
    return new Promise((resolve, reject) => {
        const errStart = (err, proc) => {
            try {
                if (err) {
                    _socket?.send(JSON.stringify({
                        status: "error",
                        message: "could not execute start",
                        error: err.toString(),
                        id: props.id
                    }));

                    reject(new Error("start: PM2 error received"));

                } else {
                    _socket?.send(JSON.stringify({
                        status: "ok",
                        cmd: "start",
                        result: {
                            proc: proc,
                        },
                        id: props.id
                    }));

                    resolve({ status: "ok" });
                }
            } catch (error) {
                console.log("errStart::error -->", error);
                reject(new Error("start error"));
            }
        }
        console.log("start:: props -->", props)
        pm2.start(props.options, errStart);
    });
}

const restart = (props) => {
    return new Promise((resolve, reject) => {
        const errRestart = (err, proc) => {
            try {
                if (err) {
                    _socket?.send(JSON.stringify({
                        status: "error",
                        message: "could not execute restart",
                        error: err.toString(),
                        id: props.id
                    }));

                    reject(new Error("restart: PM2 error received"));
                    
                } else {
                    _socket?.send(JSON.stringify({
                        status: "ok",
                        cmd: "restart",
                        result: {
                            proc: proc,
                        },
                        id: props.id
                    }));

                    resolve({ status: "ok" });
                }
            } catch (error) {
                console.log("errRestart::error -->", error);
                reject(new Error("restart error"));
            }
        }

        pm2.restart(props.name, errRestart);
    });
}

const stop = (props) => {
    return new Promise((resolve, reject) => {
        const errStop = (err, proc) => {
            try {
                if (err) {
                    _socket?.send(JSON.stringify({
                        status: "error",
                        message: "could not execute stop",
                        error: err.toString(),
                        id: props.id
                    }));

                    reject(new Error("stop: PM2 error received"));

                } else {
                    _socket?.send(JSON.stringify({
                        status: "ok",
                        cmd: "stop",
                        result: {
                            proc: proc,
                        },
                        id: props.id
                    }));

                    resolve({ status: "ok" });
                }
            } catch (error) {
                console.log("errStopt::error -->", error);
                reject(new Error("stop error"));
            }
        }
        pm2.stop(props.name, errStop);

    });
}


const del = (props) => {
    return new Promise((resolve, reject) => {
        const errDelete = (err, proc) => {
            try {
                if (err) {
                    _socket?.send(JSON.stringify({
                        status: "error",
                        message: "could not execute delete",
                        error: err.toString(),
                        id: props.id
                    }));

                    reject(new Error("delete: PM2 error received"));

                } else {
                    _socket?.send(JSON.stringify({
                        status: "ok",
                        cmd: "del",
                        result: {
                            proc: proc,
                        },
                        id: props.id
                    }));

                    resolve({ status: "ok" });
                }
            } catch (error) {
                console.log("errDelete::error -->", error);
                reject(new Error("delete error"));
            }
        }

        pm2.delete(props.name, errDelete);
        
    });
}


const callbacks = {
    'list': list,
    'start': start,
    'restart': restart,
    'stop': stop,
    'del': del,
};


module.exports = {
    callbacks: callbacks,
    setupSocket: setupSocket,
}

