const WebSocket = require('ws');
//const { port } = require('./config');
const port = process.argv.slice(2)[0];
const pm2 = require('pm2');

const { connect, setupSocket, callbacks } = require('./pm2Wrapper');

const wss = new WebSocket.Server({ port: port });
let socket;

const launchBus = (proc) => {
    try {
        console.log("Event -->", proc.event);
        console.log("Name -->", proc.process.name);
        socket?.send(JSON.stringify({
            status: "ok",
            cmd: "event",
            type: proc.event,
            name: proc.process.name,
        }));
    } catch (error) {
        console.log("launchBus::error -->", error);
    }
}

wss.on('connection', (ws) => {
    socket = ws;
    setupSocket(ws);

    //Connect to pm2
    let noDaemonMode = false;
    pm2.connect(noDaemonMode, (err) => {
        console.log("connecte");
        if (err) {
            console.log("could not connect to PM2, please restart pm2-remote.")
            return;
        }
    });

    socket.on('message', async (message) => {
        console.log("message received -->", message);
        let data;
        try {
            data = JSON.parse(message);
            let ret = await callbacks[data.cmd](data.properties);
 
            if (ret.status !== "ok") {
                console.log(ret);
                return;
            }

        } catch (error) {
            console.log("Warning: unrecognized command received");
            console.log(error);
            return;
        }
    });
})





